# Bem vindo

Esta é a parte do back-end do desafio de ferias 2021-22 oferecido pela infojr.
Tudo que você precisa está na pasta back-end.

Essa é uma api basica que permtite obter e salvar dados de um banco de dados:

## Das Rotas

A essa api entrega dados por meio de 4 rotas:

1.  /insgths/todos - entrega todos os insights da database.
2. /insights/categoria/:categoria - entrega todos os insights dentro da categoria. 
3. /insight/id/:id - entrega o insiht com aquele numero de id.
4. /insights/novoinsight - permite a criação de um insight novo.

## Das validações

Essa api faz umas validações bem amadoras, mas ao menos valida os dados que são enviados.
Categorias, titulos e descroções, caso venham como null ou como uma string vazia (''), retornam erros.

Pos sua vez as Urls e Urls alternativas não possuem validação para garatir que são urls de verdade e quando cehegam como null são substituidas por urls padrões definidas no modelo da base de dados.

Url default
Url alternativa default:

A url default é a url a imagem que foi disponibilizada no mockup do projeto infosihts.
'https://gitlab.com/EstherLuiza/atividade-de-ferias/-/raw/banco-de-dados/back-end/Default_Image/img_png.png'

A url alternativa default é a url uma imagem qualquer à la "no image availabe"
'https://freesvg.org/img/Image-Not-Found.png'


Ass: Esther Luiza Macedo Rodrigues.  