import App from './app';

const p = process.env.PORT || 3000;
App.set('port', p);

App.listen(p, () => console.log(`Server rodando na porta ${p}`)).on(
  'error',
  err => {
    console.log('Algo de errado ocorreu:', err.message);
  }
);
