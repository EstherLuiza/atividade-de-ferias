/* Oh no how embarassing */

class ApiError {
  code: number;

  message: string;

  constructor(code: number, message: string) {
    this.code = code;
    this.message = message;
  }

  static notfound = new ApiError(
    404,
    'Talvez o que você esteja procurando não existe'
  );

  static badrequest = new ApiError(
    400,
    'Alguma informação critica não foi enviada, ou foi enviada da forma errada'
  );

  static ops = new ApiError(500, 'Ops, algo de errado aconteceu');

  static databasops = new ApiError(
    500,
    'Ops, ocorreu um problema na conexão com o banco de dados'
  );
}

export default ApiError;
