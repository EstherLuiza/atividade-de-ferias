import { NextFunction, Request, Response } from 'express';
import ApiError from './error_config';

/* function ErrorHandlingfunction(err: ApiError, req: Request, res: Response) {
  const status = err.code || 500;
  const msg = err.message || 'Ops, algum erro interno aconteceu';
  res.status(status).send({ status, msg });
} */
function ErrorHandlingfunction(
  err: Error | ApiError,
  req: Request,
  res: Response,
  next: NextFunction
) {
  if (err instanceof ApiError) {
    const status = err.code;
    const msg = err.message;
    res.statusCode = status;
    res.status(status).send(next(new ApiError(status, msg)));
  }
  if (err instanceof Error) {
    res.send(next(new ApiError(500, 'Sinto muito, algo deu errado')));
  }
}
export default ErrorHandlingfunction;
