/* Esse é o arquivo que "monta" as coisas */
import express, { Application } from 'express';
import dotenv from 'dotenv';
/* importando rotas */
import all from './routes/allInsights_route';
import bytype from './routes/type_route';
import byid from './routes/id_route';
import newinsigthRoute from './routes/add_Insight_route';
import ErrorHandlingFunction from './Error/error_handler';
import errorRoute from './routes/error_route';

dotenv.config();

class App {
  public express: Application; /* public para ser utilizada em outros lugares */

  constructor() {
    this.express = express();
    this.noNameYet();
    this.rotas();
    this.ifError();
  }

  private noNameYet() {
    this.express.use(express.json());
    this.express.use(express.urlencoded({ extended: false }));
  }

  private ifError() {
    this.express.use(ErrorHandlingFunction);
  }

  rotas() {
    this.express.use(all);
    this.express.use(bytype);
    this.express.use(byid);
    this.express.use(newinsigthRoute);
    this.express.use(errorRoute);
  }
}

export default new App().express;
