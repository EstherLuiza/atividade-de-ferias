/* essa é a parte da conexão com o banco de dados */
import { createConnection } from 'mysql2/promise';

async function connectToDataBase() {
  const alo = createConnection({
    host: process.env.MYSQL_HOST,
    user: process.env.MYSQL_USER,
    port: 3306,
    password: process.env.MYSQL_PASS,
    database: process.env.MYSQL_DATABASE,
    connectionLimit: 10,
  });
  return alo;
}
export default connectToDataBase;
