/* Essa query seleciona todos os insigths da base de dados */
import ApiError from '../Error/error_config';
import connectToDataBase from './db';

async function allInsigths() {
  try {
    const connec = await connectToDataBase();
    return connec.query('SELECT * FROM ideias');
  } catch (e) {
    throw ApiError.databasops;
  }
}

export default allInsigths;
