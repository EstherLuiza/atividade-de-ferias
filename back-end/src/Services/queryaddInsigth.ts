import ApiError from '../Error/error_config';
import connectToDataBase from './db';

async function newInsigth(
  novoTitulo: string,
  novaCategoria: string,
  novoUrl: string,
  novoUrlalt: string,
  novaMensagem: string
) {
  const connec = await connectToDataBase();
  try {
    return connec.query(
      `INSERT INTO ideias (titulo,categoria,url,urlalt,mensagem) VALUES(?, ?, COALESCE(?, DEFAULT(url)), COALESCE(?, DEFAULT(urlalt)) ,?)`,
      [novoTitulo, novaCategoria, novoUrl, novoUrlalt, novaMensagem]
    );
  } catch (e) {
    throw ApiError.databasops;
  }
}

export default newInsigth;
