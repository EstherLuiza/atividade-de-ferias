/* Essa query seleciona os insigths por categoria */
/* a função uminsigth será usada no controller da rota
que entrega os insigths a depender da categoria
dada atraves dos parametros */
import ApiError from '../Error/error_config';
import connectToDataBase from './db';

async function umInsigth(categoria: string) {
  try {
    const connec = await connectToDataBase();
    return connec.query('SELECT * FROM ideias WHERE categoria = ?', categoria);
  } catch (e) {
    throw ApiError.databasops;
  }
}

export default umInsigth;
