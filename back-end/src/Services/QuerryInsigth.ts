import ApiError from '../Error/error_config';
import connectToDataBase from './db';

async function oneInsigth(id: number) {
  try {
    const connec = await connectToDataBase();
    return connec.query('SELECT * FROM ideias WHERE id = ?', id);
  } catch (e) {
    throw ApiError.databasops;
  }
}

export default oneInsigth;
