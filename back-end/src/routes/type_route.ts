import { Router } from 'express';
import typeRouteController from '../controllers/type_controller';

const router = Router();

router.get('/insights/categoria/:categoria', typeRouteController);

export default router;
