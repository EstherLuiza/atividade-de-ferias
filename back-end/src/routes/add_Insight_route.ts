import { Router } from 'express';
import newInsigthController from '../controllers/addcontroller';

const router = Router();

router.post('/insights/novoinsight', newInsigthController);

export default router;
