import { Router } from 'express';
import ErrorRouteController from '../controllers/error_route_controller';

const router = Router();

router.get('*', ErrorRouteController);
router.post('*', ErrorRouteController);
export default router;
