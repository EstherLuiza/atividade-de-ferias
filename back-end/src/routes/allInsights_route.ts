import { Router } from 'express';
import allInsigthsController from '../controllers/all_insights_controller';

const router = Router();

router.get('/insights/todos', allInsigthsController);

export default router;
