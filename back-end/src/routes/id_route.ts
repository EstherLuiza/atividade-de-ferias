import { Router } from 'express';
import oneInsigthController from '../controllers/id_controller';

const router = Router();

router.get('/insight/id/:id', oneInsigthController);

export default router;
