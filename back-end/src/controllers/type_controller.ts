/* esse é o controller da rota que entrega os insigths por categoria */
import { Request, Response } from 'express';
import Insigths from '../Services/queryCategoria';
import validator from '../Validation/validation-functions';
import ApiError from '../Error/error_config';

async function typeRouteController(req: Request, res: Response) {
  try {
    const theseInsigths = await Insigths(req.params.categoria);
    const validated = validator.Catvalidator(theseInsigths[0]);
    res.json(validated);
  } catch (e) {
    if (e instanceof ApiError) {
      res.send(e).status(e.code);
    } else if (e instanceof Error) {
      res.send(ApiError.ops).status(500);
    } else {
      res.send(ApiError.ops).status(500);
    }
  }
}

export default typeRouteController;
