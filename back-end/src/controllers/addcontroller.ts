import { Request, Response } from 'express';
import ApiError from '../Error/error_config';
import newInsigth from '../Services/queryaddInsigth';
import validators from '../Validation/validation-functions';

async function newInsigthController(req: Request, res: Response) {
  try {
    if (req.body.url === '') {
      req.body.url = null;
    }
    if (req.body.urlalt === '') {
      req.body.urlalt = null;
    }
    validators.sentValidator(
      req.body.titulo,
      req.body.categoria,
      req.body.url,
      req.body.urlalt,
      req.body.mensagem
    );
    await newInsigth(
      req.body.titulo,
      req.body.categoria,
      req.body.url,
      req.body.urlalt,
      req.body.mensagem
    );

    const recado = `Novo insigth adicionado com sucesso:
      <p>categoria: ${req.body.categoria}</p>
      <p>titulo: ${req.body.titulo} </p>
      <p>url: ${req.body.url} </p>
      <p>url alternativa: ${req.body.urlalt} </p>
      <p>descrição: ${req.body.mensagem} </p>
      Urls envidas como "null" ou "undefined" (respostas em branco) são substituidas pelas urls padrão da Api
  `;

    res.status(201).send(recado);
  } catch (e) {
    const errors = [];
    if (e instanceof ApiError) {
      errors.push(e);
      res.status(400);
    } else if (e instanceof Error) {
      errors.push(ApiError.ops);
      res.status(500);
    } else {
      errors.push(e);
      res.status(400);
    }
    res.send(errors);
  }
}

export default newInsigthController;
