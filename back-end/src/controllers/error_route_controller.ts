import { Request, Response } from 'express';
import ApiError from '../Error/error_config';

function ErrorRouteController(req: Request, res: Response) {
  const err = new ApiError(404, `A rota requisitada (${req.path}) não existe`);
  res.send(err).status(404);
}
export default ErrorRouteController;
