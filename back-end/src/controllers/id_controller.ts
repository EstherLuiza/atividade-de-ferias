import { Request, Response } from 'express';
import ApiError from '../Error/error_config';
import oneInsigth from '../Services/QuerryInsigth';
import validators from '../Validation/validation-functions';

async function oneInsigthController(req: Request, res: Response) {
  try {
    validators.idvalidator(req.params.id);
    const thisInsingth = await oneInsigth(parseInt(req.params.id, 10));
    res.send(thisInsingth[0]);
  } catch (e) {
    if (e instanceof ApiError) {
      res.send(e);
    } else {
      res.send(ApiError.ops).status(500);
    }
  }
}

export default oneInsigthController;
