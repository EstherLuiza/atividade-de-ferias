/* esse é o controller da rota que entrega todos os insgths */
import { Request, Response } from 'express';
import ApiError from '../Error/error_config';
import allIsigths from '../Services/querry_todos';

async function allInsigthsController(req: Request, res: Response) {
  try {
    const atende = await allIsigths();
    res.json(atende[0]);
  } catch (e) {
    if (e instanceof ApiError) {
      res.send(e).status(e.code);
    } else if (e instanceof Error) {
      res.send(ApiError.ops).status(500);
    } else {
      res.send(ApiError.ops);
    }
  }
}

export default allInsigthsController;
