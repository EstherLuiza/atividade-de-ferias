import ApiError from '../Error/error_config';

const validators = {
  sentValidator: function sentDataValidation(
    titulo: string,
    categoria: string,
    url: string,
    urlalt: string,
    mensagem: string
  ) {
    const validationerrors = [];
    if (!categoria || categoria === undefined || null || '') {
      validationerrors.push(new ApiError(400, 'A categoria não pode ser nula'));
    }
    if (!titulo || titulo === undefined || null || '') {
      validationerrors.push(new ApiError(400, 'O titulo não pode ser nulo'));
    }
    if (!mensagem || mensagem === undefined || null || '') {
      validationerrors.push(new ApiError(400, 'A mensagem não pode ser nula'));
    }
    if (mensagem.length > 65.535) {
      validationerrors.push(
        new ApiError(
          400,
          'Opa, o a descrição enviada possui mais caracteres do que o permitido'
        )
      );
    }
    if (url != null && url.length > 650) {
      validationerrors.push(
        new ApiError(400, 'A url enviada precisa ter menos de 650 caracteres')
      );
    }
    if (urlalt != null && urlalt.length > 650) {
      validationerrors.push(
        new ApiError(400, 'A url enviada precisa ter menos de 650 caracteres')
      );
    }
    if (validationerrors.length > 0) {
      throw validationerrors;
    }
  },
  Catvalidator: function Categoria(categoria: any) {
    if (categoria.length === 0) {
      return 'Não há insgths dentro dessa categoria';
    }
    return categoria;
  },
  idvalidator: function numberValidation(suspect: string) {
    const newNumber = parseInt(suspect, 10);
    if (Number.isNaN(newNumber)) {
      throw new ApiError(
        400,
        'Porfavor coloque um número ou tente a rota insgths/:suacategoria'
      );
    } else {
      return newNumber;
    }
  },
};

export default validators;
